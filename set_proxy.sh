#!/bin/bash

HTTP_PROXY=http://192.168.5.8:3128
HTTPS_PROXY=http://192.168.5.8:3128

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CONTAINER_DIRS=(
    ${DIR}/cinder/cinder-api
    ${DIR}/cinder/cinder-scheduler
    ${DIR}/cinder/cinder-volume
    ${DIR}/glance/glance-api
    ${DIR}/glance/glance-registry
    ${DIR}/horizon/
    ${DIR}/keystone/
    ${DIR}/masakari/masakari-api
    ${DIR}/masakari/masakari-engine
    ${DIR}/neutron/neutron-dhcp-agent
    ${DIR}/neutron/neutron-metadata-agent
    ${DIR}/neutron/neutron-ovs-agent
    ${DIR}/neutron/neutron-server
    ${DIR}/nova/nova-api
    ${DIR}/nova/nova-conductor
    ${DIR}/nova/nova-consoleauth
    ${DIR}/nova/nova-novncproxy
    ${DIR}/nova/nova-placement-api
    ${DIR}/nova/nova-scheduler
    ${DIR}/openstack-client/
)

printf "Are you under a proxy?(y/n): "
read is_proxy

for dir in ${CONTAINER_DIRS[@]};
do
    cd $dir
    if [ $is_proxy == 'y' ] || [ $is_proxy == 'Y' ]; then
        sed -e "s|HTTP_PROXY|$HTTP_PROXY|g" -e "s|HTTPS_PROXY|$HTTPS_PROXY|g" sample.proxy > Dockerfile
    elif [ $is_proxy == 'n' ] || [ $is_proxy == 'N' ]; then
        cat sample > Dockerfile
    else
        echo "Invalid answer! Exit in 2s!!"
        sleep 2
        exit 1
    fi
done
