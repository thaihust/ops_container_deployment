### 0. Set proxy and create Dockerfile with proxy setting:

- Edit `http_proxy` and `https_proxy` value in `set_proxy.sh` script, then execute:

```sh
chmod +x set_proxy.sh
./set_proxy.sh
```

- Check new docker files with proxy setting

### 1. Build images

- On Controller01, run these commands to build and then push the images to Docker private registry.

```sh
./build_images.sh

./push_images.sh
```

### 2. Run containers

- On Controller01, run this command to initialize the essential data and configurations of OpenStack environment.

```sh
./bootstrap_and_run_services.sh
```

- On the other Controller nodes, run these commands to pull latest images on the registry and then run containers of OpenStack services from them.

```sh
./pull_images.sh

./run_services.sh
```
