#!/bin/bash

source openstackclient.env
source color.env
declare -A segments
declare -A hosts
SEGMENT_NAME=""

function get_segments {
    echocolor "+ List of masakari segments: "
    while read line ;
    do
        segment_name=$(echo $line | awk '{ print $1 }')
        segment_id=$(echo $line | awk '{ print $2 }')
        if [[ ! -z $segment_name ]]; then
            segments[$segment_name]=$segment_id
        fi
    done <<< "$(masakari segment-list | awk '{ print $4, $2 }' | sed '/^\s*$/d' | egrep -v '(uuid|name)')" 
    for segment in ${!segments[@]}; do
        echo ">> ${segment}: ${segments[${segment}]}"
    done
}

function get_maintaining_hosts {
    get_segments

    printcolor "+ Choose a segment (segment name): "
    read segment_name
    while [[ -z $(echo ${!segments[*]} | grep $segment_name) ]]
    do
        printcolor "+ Warning: Segment name '$segment_name' does not exist! Please provide correct segment name: "
        read segment_name
    done 

    SEGMENT_NAME=${segment_name}
    echocolor "+ List of maintaining hosts in segment '$segment_name': "
    while read line ;
    do
        host_name=$(echo $line | awk '{ print $1 }')
        host_id=$(echo $line | awk '{ print $2 }')
        if [[ ! -z $host_name ]]; then
            hosts[$host_name]=$host_id
        fi
    done <<< "$(masakari host-list --segment-id ${segments[$segment_name]} | grep True | awk '{ print $6, $12 }')"
    for host in ${!hosts[@]}; do
        echo ">> ${host}: ${hosts[${host}]}"
    done
}

function is_enable {
    # check crm status - crm_status=$(crm_mon --as-xml | grep "node\ name\=\"$node\"\ id" | awk '{ print $4 }' | awk -F '[="]' '{ print $3 }')
    # check corosync and pacemaker status
    # restart corosync and pacemaker if disabled
    is_enable=false 
    compute_status=$(openstack compute service list | grep $1 | awk '{ print $12 }')
    if [ $compute_status == 'up' ]; then
        is_enable=true
    fi
    echo $is_enable
}

function reactive_hosts {
    get_maintaining_hosts
    printcolor "+ Pick one of the following options:\n+ 1. Reactive all maintaining hosts(default)\n+ 2. Reactive a specific host\n+ Your choice: "
    read option
    if [ $option == 1 ]; then
        for host in ${!hosts[@]}; do
            if [ `is_enable $host` == 'true' ]; then
                echocolor "+ Reactive host '$host'..."
                nova service-enable $host nova-compute
                masakari host-update --id ${hosts[${host}]} --segment-id ${segments[${SEGMENT_NAME}]} --on-maintenance False
                echocolor "+ done!"
            else
                echocolor "+ Warning: Host '$host' is shutoff or pacemaker + corosync are inactive! Please recheck host '$host' before reactive! Exit in 2s!!"
                sleep 2
            fi
        done
    elif [ $option == 2 ]; then
        printcolor "+ Enter compute host name you need to reactive: "
        read host_name
        while [[ -z $(echo ${!hosts[*]} | grep $host_name) ]]
        do
            printcolor "+ Host name '$host_name' does not exist! Please provide correct host name: "
            read host_name 
        done

        if [ `is_enable $host_name` == 'true' ]; then
            echocolor "+ Reactive host '$host_name'..."
            nova service-enable $host_name nova-compute
            masakari host-update --id ${hosts[${host_name}]} --segment-id ${segments[${SEGMENT_NAME}]} --on-maintenance False
            echocolor "+ done!"
        else
            echocolor "+ Warning: Host '$host' is shutoff or pacemaker + corosync are inactive! Please recheck host '$host' before reactive! Exit in 2s!!"
            sleep 2
        fi        
    else
        echocolor "+ Exit in 2s!!"  
        sleep 2
        exit 1
    fi
}

function main {
    reactive_hosts
}

main
