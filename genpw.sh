#!/bin/bash

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

MYSQL_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
MYSQL_PASS_SST=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

KEYSTONE_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
GLANCE_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
NOVA_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
NEUTRON_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
CINDER_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
MASAKARI_DBPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

ADMIN_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

GLANCE_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
NOVA_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
PLACEMENT_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
NEUTRON_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
CINDER_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)
MASAKARI_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

METADATA_PROXY_SHARED_SECRET=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

RABBIT_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

MEMCACHED_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

HAPROXY_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

INFLUXDB_PASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32)

# Change env for OpenStack Nodes

sed -i "s/ADMIN_PASS/$ADMIN_PASS/g" ${CUR_DIR}/run_vars/openstackclient.env

echo "ADMIN_PASS=$ADMIN_PASS" >> ${CUR_DIR}/run_vars/keystone.env
echo "KEYSTONE_DBPASS=$KEYSTONE_DBPASS" >> ${CUR_DIR}/run_vars/keystone.env

echo "GLANCE_DBPASS=$GLANCE_DBPASS" >> ${CUR_DIR}/run_vars/glance.env
echo "GLANCE_PASS=$GLANCE_PASS" >> ${CUR_DIR}/run_vars/glance.env

echo "NOVA_DBPASS=$NOVA_DBPASS" >> ${CUR_DIR}/run_vars/nova.env
echo "NOVA_PASS=$NOVA_PASS" >> ${CUR_DIR}/run_vars/nova.env
echo "PLACEMENT_PASS=$PLACEMENT_PASS" >> ${CUR_DIR}/run_vars/nova.env
echo "NEUTRON_PASS=$NEUTRON_PASS" >> ${CUR_DIR}/run_vars/nova.env
echo "METADATA_PROXY_SHARED_SECRET=$METADATA_PROXY_SHARED_SECRET" >> ${CUR_DIR}/run_vars/nova.env

echo "NEUTRON_DBPASS=$NEUTRON_DBPASS" >> ${CUR_DIR}/run_vars/neutron.env
echo "NEUTRON_PASS=$NEUTRON_PASS" >> ${CUR_DIR}/run_vars/neutron.env
echo "NOVA_PASS=$NOVA_PASS" >> ${CUR_DIR}/run_vars/neutron.env
echo "METADATA_PROXY_SHARED_SECRET=$METADATA_PROXY_SHARED_SECRET" >> ${CUR_DIR}/run_vars/neutron.env

echo "CINDER_DBPASS=$CINDER_DBPASS" >> ${CUR_DIR}/run_vars/cinder.env
echo "CINDER_PASS=$CINDER_PASS" >> ${CUR_DIR}/run_vars/cinder.env

echo "MASAKARI_DBPASS=$MASAKARI_DBPASS" >> ${CUR_DIR}/run_vars/masakari.env
echo "MASAKARI_PASS=$MASAKARI_PASS" >> ${CUR_DIR}/run_vars/masakari.env

sed -i "s/RABBIT_PASS/$RABBIT_PASS/g" ${CUR_DIR}/run_vars/general.env
sed -i "s/MEMCACHED_PASS/$MEMCACHED_PASS/g" ${CUR_DIR}/run_vars/general.env

sed -i "s/mysql_pass/$MYSQL_PASS/g" ${CUR_DIR}/create_databases.sh
sed -i "s/keystone_dbpass/$KEYSTONE_DBPASS/g" ${CUR_DIR}/create_databases.sh
sed -i "s/glance_dbpass/$GLANCE_DBPASS/g" ${CUR_DIR}/create_databases.sh
sed -i "s/nova_dbpass/$NOVA_DBPASS/g" ${CUR_DIR}/create_databases.sh
sed -i "s/neutron_dbpass/$NEUTRON_DBPASS/g" ${CUR_DIR}/create_databases.sh
sed -i "s/cinder_dbpass/$CINDER_DBPASS/g" ${CUR_DIR}/create_databases.sh

# Change env for local ansible code
sed -i "s/HAPROXY_PASS/${HAPROXY_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/INFLUXDB_PASS/${INFLUXDB_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/RABBIT_PASS/$RABBIT_PASS/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/MEMCACHED_PASS/$MEMCACHED_PASS/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/AD_PASSWD/$ADMIN_PASS/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/ID_DB_PASSWD/$KEYSTONE_DBPASS/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/IMG_DB_PASSWD/${GLANCE_DBPASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/IMG_SRV_PASSWD/${GLANCE_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/CP_DB_PASSWD/${NOVA_DBPASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/CP_SRV_PASSWD/${NOVA_PASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/PL_PASSWD/${PLACEMENT_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/META_PASSWD/${METADATA_PROXY_SHARED_SECRET}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/NW_DB_PASSWD/${NEUTRON_DBPASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/NW_SRV_PASSWD/${NEUTRON_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/BLK_DB_PASSWD/${CINDER_DBPASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/BLK_SRV_PASSWD/${CINDER_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/CPU_HA_DB_PASSWD/${MASAKARI_DBPASS}/g" ${CUR_DIR}/run_vars/all.yml
sed -i "s/CPU_HA_PASSWD/${MASAKARI_PASS}/g" ${CUR_DIR}/run_vars/all.yml

sed -i "s/mysql_pass_sst/$MYSQL_PASS_SST/g" ${CUR_DIR}/run_vars/controller.yml
sed -i "s/mysql_pass/$MYSQL_PASS/g" ${CUR_DIR}/run_vars/controller.yml
