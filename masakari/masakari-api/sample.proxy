FROM ubuntu:16.04
MAINTAINER thangmv@vcs-team

ENV SERVICE="masakari"
ENV http_proxy="HTTP_PROXY"
ENV https_proxy="HTTPS_PROXY"

#Create $service user
RUN useradd --home-dir "/var/lib/$SERVICE" \
        --create-home \
        --system \
        --shell /bin/false \
        $SERVICE

#Create and set ownership of essential dirs
RUN mkdir -p /var/log/$SERVICE \
        && mkdir -p /etc/$SERVICE \
        && chown -R $SERVICE:$SERVICE /var/log/$SERVICE \
        && chown -R $SERVICE:$SERVICE /var/lib/$SERVICE \
        && chown -R $SERVICE:$SERVICE /etc/$SERVICE

RUN apt update \
	&& apt install -y git python-pip python-pymysql python-memcache crudini vim \
	&& apt clean

RUN git clone https://github.com/openstack/masakari -b stable/ocata

COPY database.py /usr/local/bin/database.py

RUN cd /masakari \
    && cat /usr/local/bin/database.py > masakari/conf/database.py \
    && pip install -r requirements.txt . \
    && cp etc/masakari/* /etc/masakari/

COPY masakari.conf /etc/masakari/masakari.conf
COPY bootstrap.sh /usr/local/bin/bootstrap.sh
COPY start_masakari-api.sh /usr/local/bin/start_masakari-api.sh
COPY change_volume_perms.sh /usr/local/bin/change_volume_perms.sh

RUN chmod +x /usr/local/bin/*.sh \
	&& rm -rf /masakari \
        && echo "masakari ALL=(root) NOPASSWD: /usr/local/bin/change_volume_perms.sh" >> /etc/sudoers

EXPOSE 15868

USER masakari

CMD ["start_masakari-api.sh"]
