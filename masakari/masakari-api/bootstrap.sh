#!/bin/sh

sed -i "s|MY_IP|$MY_IP|g" /etc/masakari/masakari.conf
sed -i "s|NOVA_PASS|$NOVA_PASS|g" /etc/masakari/masakari.conf
sed -i "s|TRANSPORT_URL|$TRANSPORT_URL|g" /etc/masakari/masakari.conf
sed -i "s|CONTROLLER_HOST|$CONTROLLER_HOST|g" /etc/masakari/masakari.conf
sed -i "s|MASAKARI_PASS|$MASAKARI_PASS|g" /etc/masakari/masakari.conf
sed -i "s|MASAKARI_DBPASS|$MASAKARI_DBPASS|g" /etc/masakari/masakari.conf
sed -i "s|DB_HOST|$DB_HOST|g" /etc/masakari/masakari.conf
sed -i "s|MEMCACHED_SERVERS|$MEMCACHED_SERVERS|g" /etc/masakari/masakari.conf
sed -i "s|RABBIT_HOSTS|$RABBIT_HOSTS|g" /etc/masakari/masakari.conf
sed -i "s|MSQ_PASS|$MSQ_PASS|g" /etc/masakari/masakari.conf

masakari-manage db sync
