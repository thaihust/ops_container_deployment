FROM ubuntu:16.04
MAINTAINER thangmv@vcs-team

ENV SERVICE="masakari"

#Create $service user
RUN useradd --home-dir "/var/lib/$SERVICE" \
        --create-home \
        --system \
        --shell /bin/false \
        $SERVICE

#Create and set ownership of essential dirs
RUN mkdir -p /var/log/$SERVICE \
        && mkdir -p /etc/$SERVICE \
        && chown -R $SERVICE:$SERVICE /var/log/$SERVICE \
        && chown -R $SERVICE:$SERVICE /var/lib/$SERVICE \
        && chown -R $SERVICE:$SERVICE /etc/$SERVICE

RUN apt update \
	&& apt install -y git python-pip python-pymysql python-memcache crudini vim \
	&& apt clean

RUN git clone https://github.com/openstack/masakari -b stable/ocata

COPY database.py /usr/local/bin/database.py

RUN cd /masakari \
    && cat /usr/local/bin/database.py > masakari/conf/database.py \
    && pip install -r requirements.txt . \
    && cp etc/masakari/* /etc/masakari/

COPY masakari.conf /etc/masakari/
COPY start_masakari-engine.sh /usr/local/bin/
COPY change_volume_perms.sh /usr/local/bin/change_volume_perms.sh

RUN chmod +x /usr/local/bin/*.sh \
	&& rm -rf /masakari \
        && echo "masakari ALL=(root) NOPASSWD: /usr/local/bin/change_volume_perms.sh" >> /etc/sudoers

USER masakari

CMD ["start_masakari-engine.sh"]
